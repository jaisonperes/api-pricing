import { DocumentBuilder } from '@nestjs/swagger';
import mainDocs from './info';

const documentBuilder = new DocumentBuilder()
  .setTitle(mainDocs.title)
  .setDescription(mainDocs.description)
  .setVersion(mainDocs.version)
  .addTag(mainDocs.docs.AUTH_API.TITLE, mainDocs.docs.AUTH_API.DESCRIPTION)
  .addTag(mainDocs.docs.DEAL_API.TITLE, mainDocs.docs.DEAL_API.DESCRIPTION)
  .addTag(mainDocs.docs.CLIENT_API.TITLE, mainDocs.docs.CLIENT_API.DESCRIPTION)
  .addTag(
    mainDocs.docs.PRODUCT_API.TITLE,
    mainDocs.docs.PRODUCT_API.DESCRIPTION,
  )
  .addTag(mainDocs.docs.USER_API.TITLE, mainDocs.docs.USER_API.DESCRIPTION)
  .addBearerAuth();

export const SwaggerOptions = documentBuilder.build();
