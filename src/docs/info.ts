const docs = {
  AUTH_API: {
    TITLE: 'Auth',
    DESCRIPTION: 'API para autenticação de usuários',
  },
  DEAL_API: {
    TITLE: 'Deal',
    DESCRIPTION: 'API para gestão de contratos',
  },
  PNL_API: {
    TITLE: 'PnL',
    DESCRIPTION: 'API para cálculo de PnL',
  },
  PROMO_API: {
    TITLE: 'Promo',
    DESCRIPTION: 'API para gestão de promoções',
  },
  CLIENT_API: {
    TITLE: 'Client',
    DESCRIPTION: 'API para gestão de clientes',
  },
  USER_API: {
    TITLE: 'User',
    DESCRIPTION: 'API para gestão de usuários',
  },
  PRODUCT_API: {
    TITLE: 'Product',
    DESCRIPTION: 'API para gestão de produtos',
  },
};

const title = 'Api Pricing';
const description =
  'API de suporte para negociação de contratos e cálculo de PnL <br>' +
  '<br>' +
  'Baixe a documentação em <a href="http://localhost:3000/docs-json" download>docs-json</a> e importe em seu Cliente HTTP favorito 💜';
const version = '1.0';

export default {
  docs,
  title,
  description,
  version,
};
