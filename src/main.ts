import { LogLevel, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { TransformInterceptor } from './transform.interceptor';
import { SwaggerOptions } from './docs/swaggerOptions';
import { SwaggerModule } from '@nestjs/swagger';
import { AuthModule } from './app/auth/auth.module';
import { DealModule } from './app/deal/deal.module';
import { PnlModule } from './app/pnl/pnl.module';
import { PromoModule } from './app/promo/promo.module';
import { UsersModule } from './app/users/users.module';
import { ClientsModule } from './app/clients/clients.module';
import { ProductsModule } from './app/products/products.module';

type LogLevelList = LogLevel[];

async function bootstrap() {
  const LOG_LEVEL: LogLevelList =
    process.env.LOG_LEVEL === 'debug' ? ['debug', 'log'] : ['error'];

  const app = await NestFactory.create(AppModule, {
    logger: LOG_LEVEL,
  });

  app.enableCors();

  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new TransformInterceptor());

  const document = SwaggerModule.createDocument(app, SwaggerOptions, {
    include: [
      AuthModule,
      DealModule,
      PnlModule,
      PromoModule,
      UsersModule,
      ClientsModule,
      ProductsModule,
    ],
  });
  SwaggerModule.setup('docs', app, document);

  const port = process.env.PORT || 3000;
  await app.listen(port);
}
bootstrap();
