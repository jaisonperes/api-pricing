import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { DealEntity } from '../deal/deal.entity';
import { BillingType } from './enum/billing-type.enum';

@Entity({ name: 'products' })
export class ProductEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt: Date;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column('enum', { enum: BillingType, name: 'billing_type' })
  billingType: BillingType;

  @Column({ name: 'fab_cost_per_unit' })
  fabCostPerUnit: number;

  @Column({ name: 'transport_cost_per_unit' })
  transportCostPerUnit: number;

  @Column({ name: 'tax_per_unit' })
  taxPerUnit: number;

  @Column()
  price: number;

  /**
   * Relations
   */

  @OneToMany(() => DealEntity, (deal) => deal.user, { eager: false })
  deals: DealEntity[];
}
