import { IsEnum, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { BillingType } from '../enum/billing-type.enum';
import { MessagesHelper } from 'src/helpers';

export class SaveProductDto {
  @IsNotEmpty({
    message: MessagesHelper.PRODUCT_NAME_REQUIRED,
  })
  @ApiProperty({
    type: String,
    description: 'Product name',
    example: 'Pedra da Sorte',
  })
  name: string;

  @IsNotEmpty({
    message: MessagesHelper.PRODUCT_DESCRIPTION_REQUIRED,
  })
  @ApiProperty({
    type: String,
    description: 'Product description',
    example: 'A Pedra da Sorte é um item de valor imensamente elevado',
  })
  description: string;

  @IsNotEmpty({
    message: MessagesHelper.PRODUCT_BILLING_TYPE_REQUIRED,
  })
  @IsEnum(BillingType, {
    message: MessagesHelper.PRODUCT_BILLING_TYPE_INVALID,
  })
  @ApiProperty({
    type: String,
    description: `Product billing type. One of the following: ${Object.values(
      BillingType,
    )}`,
    example: BillingType.RECURRENCE,
  })
  billingType: BillingType;

  @IsNotEmpty({
    message: MessagesHelper.PRODUCT_FAB_COST_REQUIRED,
  })
  @IsNumber()
  @ApiProperty({
    type: Number,
    description: 'Product fabrication cost',
    example: 100,
  })
  fabCostPerUnit: number;

  @IsNotEmpty({
    message: MessagesHelper.PRODUCT_TRANSPORT_COST_REQUIRED,
  })
  @IsNumber()
  @ApiProperty({
    type: Number,
    description: 'Product transport cost',
    example: 10,
  })
  transportCostPerUnit: number;

  @IsNotEmpty({
    message: MessagesHelper.PRODUCT_TAX_REQUIRED,
  })
  @IsNumber()
  @ApiProperty({
    type: Number,
    description: 'Product tax per unit',
    example: 10,
  })
  taxPerUnit: number;

  @IsNotEmpty({
    message: MessagesHelper.PRODUCT_PRICE_REQUIRED,
  })
  @IsNumber()
  @ApiProperty({
    type: Number,
    description: 'Product price',
    example: 100,
  })
  price: number;
}
