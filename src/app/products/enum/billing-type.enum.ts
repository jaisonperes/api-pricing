export enum BillingType {
  RECURRENCE = 'RECURRENCE',
  UNIQUE = 'UNIQUE',
  ON_DEMAND = 'ON_DEMAND',
}
