import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MessagesHelper } from 'src/helpers';
import { Repository } from 'typeorm';
import { SaveProductDto } from './dto/product.dto';
import { ProductEntity } from './product.entity';

@Injectable()
export class ProductsService {
  private readonly logger = new Logger(ProductsService.name);
  constructor(
    @InjectRepository(ProductEntity)
    private readonly productsRepository: Repository<ProductEntity>,
  ) {}

  async find(): Promise<ProductEntity[]> {
    return await this.productsRepository.find();
  }

  async findOne(id: string): Promise<ProductEntity> {
    try {
      return await this.productsRepository.findOneOrFail({ where: { id } });
    } catch (error) {
      this.logger.error('Product not found');
      throw new NotFoundException(MessagesHelper.PRODUCT_NOT_FOUND);
    }
  }

  async create(data: SaveProductDto): Promise<ProductEntity> {
    const createdAt = new Date();
    const product = this.productsRepository.create({
      ...data,
      createdAt,
      updatedAt: createdAt,
    });
    return await this.productsRepository.save(product);
  }

  async update(id: string, data: SaveProductDto): Promise<ProductEntity> {
    const product = await this.findOne(id);
    await this.productsRepository.merge(product, data);
    return await this.productsRepository.save(product);
  }

  async delete(id: string): Promise<void> {
    await this.findOne(id);
    this.logger.log(`Product found with id: ${id}`);
    await this.productsRepository.softDelete({ id });
  }
}
