import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SaveProductDto } from './dto/product.dto';
import { ProductEntity } from './product.entity';
import { ProductsService } from './products.service';

@Controller('products')
@UseGuards(AuthGuard('jwt'))
@ApiTags('Product')
@ApiBearerAuth()
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get()
  findAll(): Promise<ProductEntity[]> {
    return this.productsService.find();
  }

  @Get('/:id')
  findOne(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<ProductEntity> {
    return this.productsService.findOne(id);
  }

  @Post()
  create(@Body() data: SaveProductDto): Promise<ProductEntity> {
    return this.productsService.create(data);
  }

  @Put('/:id')
  update(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() data: SaveProductDto,
  ): Promise<ProductEntity> {
    return this.productsService.update(id, data);
  }

  @Delete('/:id')
  remove(id: string): Promise<void> {
    return this.productsService.delete(id);
  }
}
