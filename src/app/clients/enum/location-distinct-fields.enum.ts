export enum LocationDistinctFields {
  district = 'district',
  city = 'city',
  state = 'state',
  country = 'country',
  inference = 'inference',
}
