import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { DealEntity } from '../../deal/deal.entity';
import { ClientSegmentEntity, ClientLocationEntity } from '.';

@Entity({ name: 'clients' })
export class ClientEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt: Date;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  phone: string;

  @Column()
  tpv: number;

  /**
   * Relations
   */

  @ManyToOne(() => ClientSegmentEntity, (segment) => segment.client, {
    eager: true,
  })
  segment: ClientSegmentEntity;

  @ManyToOne(() => ClientLocationEntity, (location) => location.client, {
    eager: true,
  })
  location: ClientLocationEntity;

  @OneToMany(() => DealEntity, (deal) => deal.client, { eager: false })
  deal: DealEntity;
}
