import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from '../../users/users.entity';
import { ClientEntity } from '.';

@Entity({ name: 'segments' })
export class ClientSegmentEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt: Date;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  inference: number;

  /**
   * Relations
   */

  @OneToMany(() => ClientEntity, (client) => client.segment, { eager: false })
  client: UserEntity;
}
