import { IsNotEmpty, Matches } from 'class-validator';
import { MessagesHelper, ReGexHelper } from 'src/helpers';
import { ApiProperty } from '@nestjs/swagger';

export class SaveClientDto {
  @IsNotEmpty({
    message: 'Client name is required',
  })
  @ApiProperty({
    type: String,
    description: 'New user name',
    example: 'John Doe',
  })
  name: string;

  @IsNotEmpty({
    message: MessagesHelper.EMAIL_REQUIRED,
  })
  @Matches(ReGexHelper.email, {
    message: MessagesHelper.EMAIL_INVALID,
  })
  @ApiProperty({
    type: String,
    description: 'New user email',
    example: 'johndoe@mail.com',
  })
  email: string;

  @IsNotEmpty({
    message: MessagesHelper.PHONE_REQUIRED,
  })
  @Matches(ReGexHelper.phone, {
    message: MessagesHelper.PHONE_INVALID,
  })
  @ApiProperty({
    type: Number,
    description: 'New user phone (only numbers)',
    example: '01198456789',
  })
  phone: string;

  @IsNotEmpty({
    message: MessagesHelper.TPV_REQUIRED,
  })
  @ApiProperty({
    type: Number,
    description: 'New user TPV (Total Payment Volume) number in currency',
    example: 10000,
  })
  tpv: number;

  @IsNotEmpty({
    message: MessagesHelper.CLIENT_SEGMENT_REQUIRED,
  })
  @ApiProperty({
    type: String,
    description: 'Client segment uuid',
    example: '12345678-1234-1234-1234-1234567890ab',
  })
  segmentId: string;

  @IsNotEmpty({
    message: MessagesHelper.CLIENT_LOCATION_REQUIRED,
  })
  @ApiProperty({
    type: String,
    description: 'Client location uuid',
    example: '12345678-1234-1234-1234-1234567890ab',
  })
  locationId: string;
}
