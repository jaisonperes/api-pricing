import { IsEnum, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { LocationDistinctFields } from '../enum/location-distinct-fields.enum';

export class FindLocationDistinctDto {
  @IsNotEmpty({
    message: 'Location distinct field is required',
  })
  @IsEnum(LocationDistinctFields, {
    message: `Location distinct field must be one of: ${Object.values(
      LocationDistinctFields,
    ).join(', ')}`,
  })
  @ApiProperty({
    type: String,
    description: `Location distinct field: ${Object.values(
      LocationDistinctFields,
    ).join(', ')}`,
    example: 'state',
  })
  field: LocationDistinctFields;
}
