import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SaveClientLocationDto {
  @IsNotEmpty({
    message: 'Location district is required',
  })
  @ApiProperty({
    type: String,
    description: 'New client location district',
    example: 'Ipanema',
  })
  district: string;
  @IsNotEmpty({
    message: 'Location city is required',
  })
  @ApiProperty({
    type: String,
    description: 'New client location city',
    example: 'Rio de Janeiro',
  })
  city: string;

  @IsNotEmpty({
    message: 'Location state is required',
  })
  @ApiProperty({
    type: String,
    description: 'New client location state',
    example: 'RJ',
  })
  state: string;

  @IsNotEmpty({
    message: 'Location country is required',
  })
  @ApiProperty({
    type: String,
    description: 'New client location country',
    example: 'BR',
  })
  country: string;

  @IsNotEmpty({
    message: 'Location inference is required',
  })
  @ApiProperty({
    type: Number,
    description: 'New client location inference percent: 0-100',
    example: 0.5,
  })
  inference: number;
}
