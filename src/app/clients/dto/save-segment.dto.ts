import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SaveClientSegmentDto {
  @IsNotEmpty({
    message: 'Client segment name is required',
  })
  @ApiProperty({
    type: String,
    description: 'New Client Segment name',
    example: 'Armazém de Variedades',
  })
  name: string;

  @IsNotEmpty({
    message: 'Client Segment description is required',
  })
  @ApiProperty({
    type: String,
    description: 'New Client Segment description',
    example: 'Produtos diversos',
  })
  description: string;

  @IsNotEmpty({
    message: 'Client Segment inference is required',
  })
  @ApiProperty({
    type: Number,
    description: 'New Client Segment inference percent: 0-100',
    example: 5,
  })
  inference: number;
}
