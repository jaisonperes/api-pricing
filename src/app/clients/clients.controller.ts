import {
  Body,
  Controller,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ClientsService } from './clients.service';
import { FindLocationDistinctDto } from './dto/find-location-distinct.dto';
import { SaveClientDto } from './dto/save-client.dto';
import { SaveClientLocationDto } from './dto/save-location.dto';
import { SaveClientSegmentDto } from './dto/save-segment.dto';
import {
  ClientEntity,
  ClientLocationEntity,
  ClientSegmentEntity,
} from './entities';

@Controller('clients')
@UseGuards(AuthGuard('jwt'))
@ApiTags('Client')
@ApiBearerAuth()
export class ClientsController {
  constructor(private readonly clientsService: ClientsService) {}

  // Client Location Services

  @Get('locations')
  findAllLocations(): Promise<ClientLocationEntity[]> {
    return this.clientsService.findAllLocations();
  }

  @Get('locations/:field')
  async findLocationDistincts(
    @Param() findDistinctDto: FindLocationDistinctDto,
  ): Promise<string[]> {
    return await this.clientsService.findLocationDistincts(findDistinctDto);
  }

  @Post('locations')
  async createLocation(
    @Body() location: SaveClientLocationDto,
  ): Promise<ClientLocationEntity> {
    return await this.clientsService.createLocation(location);
  }

  // Client Segment Services

  @Get('segments')
  findAllSegments(): Promise<ClientSegmentEntity[]> {
    return this.clientsService.findAllSegments();
  }

  @Get('segments/name')
  findAllSegmentsDistincts(): Promise<string[]> {
    return this.clientsService.findAllSegmentsDistincts();
  }

  @Post('segments')
  async createSegment(
    @Body() segment: SaveClientSegmentDto,
  ): Promise<ClientSegmentEntity> {
    return await this.clientsService.createSegment(segment);
  }

  @Get()
  async findAll(): Promise<ClientEntity[]> {
    return await this.clientsService.findAll();
  }

  @Get('/:id')
  async findOne(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<ClientEntity> {
    return await this.clientsService.findOne({ where: { id } });
  }

  @Post()
  async createClient(@Body() client: SaveClientDto): Promise<ClientEntity> {
    return await this.clientsService.create(client);
  }
}
