import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsService } from './clients.service';
import { ClientsController } from './clients.controller';
import {
  ClientEntity,
  ClientLocationEntity,
  ClientSegmentEntity,
} from './entities';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ClientEntity,
      ClientLocationEntity,
      ClientSegmentEntity,
    ]),
  ],
  providers: [ClientsService],
  controllers: [ClientsController],
  exports: [ClientsService],
})
export class ClientsModule {}
