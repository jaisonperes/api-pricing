import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MessagesHelper } from 'src/helpers';
import { FindOneOptions, Repository } from 'typeorm';
import { FindLocationDistinctDto } from './dto/find-location-distinct.dto';
import { SaveClientDto } from './dto/save-client.dto';
import { SaveClientLocationDto } from './dto/save-location.dto';
import { SaveClientSegmentDto } from './dto/save-segment.dto';
import {
  ClientEntity,
  ClientLocationEntity,
  ClientSegmentEntity,
} from './entities';

@Injectable()
export class ClientsService {
  private readonly logger = new Logger(ClientsService.name);
  constructor(
    @InjectRepository(ClientEntity)
    private readonly clientRepository: Repository<ClientEntity>,
    @InjectRepository(ClientLocationEntity)
    private readonly clientLocationRepository: Repository<ClientLocationEntity>,
    @InjectRepository(ClientSegmentEntity)
    private readonly clientSegmentRepository: Repository<ClientSegmentEntity>,
  ) {}

  async findAll(): Promise<ClientEntity[]> {
    return await this.clientRepository.find();
  }

  async findOne(options: FindOneOptions<ClientEntity>): Promise<ClientEntity> {
    try {
      return await this.clientRepository.findOneOrFail(options);
    } catch (error) {
      this.logger.error('User not found');
      throw new NotFoundException(MessagesHelper.USER_NOT_FOUND);
    }
  }

  async create(data: SaveClientDto): Promise<ClientEntity> {
    const createdAt = new Date();
    const client = this.clientRepository.create({
      ...data,
      createdAt,
      updatedAt: createdAt,
      segment: { id: data.segmentId },
      location: { id: data.locationId },
    });
    return await this.clientRepository.save(client);
  }

  async delete(id: string): Promise<void> {
    await this.clientRepository.softDelete({ id });
  }

  async findAllLocations(): Promise<ClientLocationEntity[]> {
    return await this.clientLocationRepository.find();
  }

  async findLocationDistincts(
    findDistinctDto: FindLocationDistinctDto,
  ): Promise<string[]> {
    const { field } = findDistinctDto;
    const districts = await this.clientLocationRepository
      .createQueryBuilder('location')
      .distinctOn([`location.${field}`])
      .getRawMany();

    return districts.map((district) => district[`location_${field}`]);
  }

  async createLocation(
    data: SaveClientLocationDto,
  ): Promise<ClientLocationEntity> {
    const createdAt = new Date();
    const location = this.clientLocationRepository.create({
      ...data,
      createdAt,
      updatedAt: createdAt,
    });
    return await this.clientLocationRepository.save(location);
  }

  async findAllSegments(): Promise<ClientSegmentEntity[]> {
    return await this.clientSegmentRepository.find();
  }

  async findAllSegmentsDistincts(): Promise<string[]> {
    const segments = await this.clientSegmentRepository
      .createQueryBuilder('segment')
      .distinctOn(['segment.name'])
      .getRawMany();

    return segments.map((segment) => segment.segment_name);
  }

  async createSegment(
    data: SaveClientSegmentDto,
  ): Promise<ClientSegmentEntity> {
    const createdAt = new Date();
    const segment = this.clientSegmentRepository.create({
      ...data,
      createdAt,
      updatedAt: createdAt,
      inference: parseFloat(`${data.inference}`),
    });
    return await this.clientSegmentRepository.save(segment);
  }
}
