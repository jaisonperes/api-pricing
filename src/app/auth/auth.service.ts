import { ForbiddenException, Injectable, Logger } from '@nestjs/common';
import { UserEntity } from '../users/users.entity';
import { UsersService } from '../users/users.service';
import { compareSync } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { UserStatusEnum } from '../users/enum/user-status.enum';
import { AuthLoginDto } from './dto/auth-login.dto';
import { AuthUser } from './types/auth-request.types';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async login(user: AuthUser, authLoginDto: AuthLoginDto) {
    if (user.email !== authLoginDto.email) {
      throw new ForbiddenException('Email is not valid');
    }

    const payload = {
      sub: user.id,
      email: user.email,
      status: user.status,
    };

    return {
      token: this.jwtService.sign(payload),
    };
  }

  async validateUser(email: string, password: string) {
    let userFounded: UserEntity;
    try {
      userFounded = await this.userService.findOne({ where: { email } });
    } catch (error) {
      return null;
    }

    const isValidPassword = await compareSync(password, userFounded.password);
    if (!isValidPassword) return null;
    if (userFounded.status !== UserStatusEnum.APPROVED) return null;

    return userFounded;
  }
}
