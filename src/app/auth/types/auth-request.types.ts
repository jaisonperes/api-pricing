export interface AuthRequest extends Request {
  user: AuthUser;
}

export type AuthUser = {
  id: string;
  email: string;
  status: string;
};
