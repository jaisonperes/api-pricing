import { IsNotEmpty, Matches } from 'class-validator';
import { MessagesHelper, ReGexHelper } from 'src/helpers';
import { ApiProperty } from '@nestjs/swagger';

export class AuthLoginDto {
  @IsNotEmpty({
    message: MessagesHelper.EMAIL_REQUIRED,
  })
  @Matches(ReGexHelper.email, {
    message: MessagesHelper.EMAIL_INVALID,
  })
  @ApiProperty({
    type: String,
    description: 'Auth user email',
    example: 'johndoe@mail.com',
  })
  email: string;

  @IsNotEmpty({
    message: MessagesHelper.PASSWORD_REQUIRED,
  })
  @Matches(ReGexHelper.password, {
    message: MessagesHelper.PASSWORD_INVALID,
  })
  @ApiProperty({
    type: String,
    description: 'Auth user password',
    example: 'someStrongPassword',
  })
  password: string;
}
