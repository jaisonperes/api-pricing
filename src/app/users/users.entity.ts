import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserStatusEnum } from './enum/user-status.enum';
import { hashSync } from 'bcrypt';
import { DealEntity } from '../deal/deal.entity';
import { UserRoleEnum } from './enum/user-role.enum';

const SALT_ROUNDS = 10;

@Entity({ name: 'users' })
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at', type: 'timestamp' })
  deletedAt: Date;

  @Column({ name: 'first_name' })
  firstName: string;

  @Column({ name: 'last_name' })
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column('enum', { enum: UserStatusEnum })
  status: UserStatusEnum = UserStatusEnum.PENDING;

  @Column('enum', { enum: UserRoleEnum })
  role: UserRoleEnum = UserRoleEnum.SALLER;

  @BeforeInsert()
  createHashPassword() {
    this.password = hashSync(this.password, SALT_ROUNDS);
  }

  /**
   * Relations
   */

  @OneToMany(() => DealEntity, (deal) => deal.user, { eager: true })
  deals: DealEntity[];
}
