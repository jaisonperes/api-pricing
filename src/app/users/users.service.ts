import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MessagesHelper } from 'src/helpers/messages.helper';
import { FindOneOptions, Repository } from 'typeorm';
import { DealStatusEnum } from '../deal/enum/deal-status.enum';
import { SaveUserDto } from './dto/save-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserSalesXpEnum } from './enum/user-sales-xp.enum';
import { UserXpResponse } from './types/user-experience.types';
import { UserEntity } from './users.entity';

const SALES_XP_RANGES = {
  [UserSalesXpEnum.BEGINNER]: {
    dealsApproved: {
      min: 0,
      max: 5,
    },
    dealsProfAverage: {
      min: 0,
      max: 30,
    },
  },
  [UserSalesXpEnum.INTERMEDIATE]: {
    dealsApproved: {
      min: 5,
      max: 10,
    },
    dealsProfAverage: {
      min: 30,
      max: 50,
    },
  },
  [UserSalesXpEnum.ADVANCED]: {
    dealsApproved: {
      min: 10,
      max: 20,
    },
    dealsProfAverage: {
      min: 50,
      max: 70,
    },
  },
  [UserSalesXpEnum.EXPERT]: {
    dealsApproved: {
      min: 20,
      max: 30,
    },
    dealsProfAverage: {
      min: 70,
      max: 100,
    },
  },
};

@Injectable()
export class UsersService {
  private readonly logger = new Logger(UsersService.name);

  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
  ) {}

  async find(): Promise<UserEntity[]> {
    return await this.usersRepository.find({
      select: [
        'id',
        'firstName',
        'lastName',
        'email',
        'createdAt',
        'updatedAt',
        'status',
      ],
    });
  }
  async findOne(options: FindOneOptions<UserEntity>): Promise<UserEntity> {
    try {
      return await this.usersRepository.findOneOrFail(options);
    } catch (error) {
      this.logger.error('User not found');
      throw new NotFoundException(MessagesHelper.USER_NOT_FOUND);
    }
  }
  async create(data: SaveUserDto): Promise<UserEntity> {
    const userExists = await this.findOne({ where: { email: data.email } });

    if (userExists) {
      this.logger.error('User already exists');
      throw new BadRequestException(MessagesHelper.USER_ALREADY_EXISTS);
    }

    const createdAt = new Date();
    const user = this.usersRepository.create({
      ...data,
      createdAt,
      updatedAt: createdAt,
    });
    return await this.usersRepository.save(user);
  }
  async update(id: string, data: UpdateUserDto): Promise<UserEntity> {
    const user = await this.findOne({ where: { id } });
    await this.usersRepository.merge(user, data);
    return await this.usersRepository.save(user);
  }
  async delete(id: string): Promise<void> {
    await this.findOne({ where: { id } });
    this.logger.log(`User found with id: ${id}`);
    await this.usersRepository.softDelete({ id });
  }

  async getUserSalesXp(id: string): Promise<UserXpResponse> {
    const user = await this.findOne({ where: { id } });

    if (!user.deals?.length) {
      return {
        experience: UserSalesXpEnum.BEGINNER,
        dealStats: {
          approved: 0,
          pending: 0,
          rejected: 0,
          profitabilityRatingAverage: 0,
        },
      };
    }

    const dealsApproved = await user.deals.filter(
      (deal) => deal.status === DealStatusEnum.APPROVED,
    );

    const dealsPending = await user.deals.filter(
      (deal) => deal.status === DealStatusEnum.PENDING,
    );

    const dealsRejected = await user.deals.filter(
      (deal) => deal.status === DealStatusEnum.REJECTED,
    );

    const profitabilityAverage = dealsApproved.reduce(
      (acc, deal) => acc + deal.profitabilityRating,
      0,
    );

    const salesDealsAverage = profitabilityAverage / dealsApproved.length;

    // Find experience label based on salesDealsAverage average and total deals approved
    const experience = Object.keys(SALES_XP_RANGES).find((key) => {
      const quality =
        SALES_XP_RANGES[key].dealsProfAverage.min <= salesDealsAverage &&
        SALES_XP_RANGES[key].dealsProfAverage.max >= salesDealsAverage;

      const quantity =
        SALES_XP_RANGES[key].dealsApproved.min <= dealsApproved.length &&
        SALES_XP_RANGES[key].dealsApproved.max >= dealsApproved.length;

      return quality || quantity;
    }) as UserSalesXpEnum;

    return {
      experience: experience || UserSalesXpEnum.BEGINNER,
      dealStats: {
        approved: dealsApproved.length,
        pending: dealsPending.length,
        rejected: dealsRejected.length,
        profitabilityRatingAverage: salesDealsAverage,
      },
    };
  }
}
