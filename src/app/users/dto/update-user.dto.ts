import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { UserStatusEnum } from '../enum/user-status.enum';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateUserDto {
  @IsNotEmpty({
    message: 'User first name is required',
  })
  @IsOptional()
  @ApiProperty({
    type: String,
    description: 'User first name',
    example: 'John',
  })
  firstName: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    type: String,
    description: 'User last name',
    example: 'Doe',
  })
  lastName: string;

  @IsEnum(UserStatusEnum, {
    message:
      'User status must be one of the following: PENDING, ACTIVE, INACTIVE',
  })
  @IsOptional()
  @ApiProperty({
    type: String,
    description: `User status. One of the following: ${Object.values(
      UserStatusEnum,
    )}`,
    example: UserStatusEnum.PENDING,
  })
  status: UserStatusEnum;
}
