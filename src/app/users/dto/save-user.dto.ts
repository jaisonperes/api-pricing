import { IsNotEmpty, IsOptional, IsString, Matches } from 'class-validator';
import { MessagesHelper, ReGexHelper } from 'src/helpers';
import { ApiProperty } from '@nestjs/swagger';

export class SaveUserDto {
  @IsNotEmpty({
    message: MessagesHelper.FIRST_NAME_REQUIRED,
  })
  @ApiProperty({
    type: String,
    description: 'User first name',
    example: 'John',
  })
  firstName: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    type: String,
    description: 'User last name',
    example: 'Doe',
  })
  lastName: string;

  @IsNotEmpty({
    message: MessagesHelper.EMAIL_REQUIRED,
  })
  @Matches(ReGexHelper.email, {
    message: MessagesHelper.EMAIL_INVALID,
  })
  @ApiProperty({
    type: String,
    description: 'User email',
    example: 'johndoe@mail.com',
  })
  email: string;

  @IsNotEmpty({
    message: MessagesHelper.PASSWORD_REQUIRED,
  })
  @Matches(ReGexHelper.password, {
    message: MessagesHelper.PASSWORD_INVALID,
  })
  @ApiProperty({
    type: String,
    description: 'User password',
    example: 'someStrongPassword',
  })
  password: string;
}
