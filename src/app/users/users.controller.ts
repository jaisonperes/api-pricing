import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SaveUserDto } from './dto/save-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './users.entity';
import { UsersService } from './users.service';

@Controller('users')
@ApiTags('User')
@ApiBearerAuth()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async findAllUsers(): Promise<UserEntity[]> {
    return await this.usersService.find();
  }

  @Post()
  async createUser(@Body() data: SaveUserDto) {
    return await this.usersService.create(data);
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  async findOneUsers(
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<UserEntity> {
    return await this.usersService.findOne({ where: { id } });
  }

  @Put('/:id')
  @UseGuards(AuthGuard('jwt'))
  async updateOneUser(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() data: UpdateUserDto,
  ): Promise<UserEntity> {
    return await this.usersService.update(id, data);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.NO_CONTENT)
  async deleteOneUser(@Param('id', new ParseUUIDPipe()) id: string) {
    return await this.usersService.delete(id);
  }

  @Get('/xp/:id')
  @UseGuards(AuthGuard('jwt'))
  async getUserXp(@Param('id', new ParseUUIDPipe()) id: string) {
    return await this.usersService.getUserSalesXp(id);
  }
}
