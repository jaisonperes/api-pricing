import { UserSalesXpEnum } from '../enum/user-sales-xp.enum';

export type UserXpResponse = {
  experience: keyof typeof UserSalesXpEnum;
  dealStats: {
    approved: number;
    pending: number;
    rejected: number;
    profitabilityRatingAverage: number;
  };
};
