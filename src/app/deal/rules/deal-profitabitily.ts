import { UserSalesXpEnum } from 'src/app/users/enum/user-sales-xp.enum';

export const TPV_RANGE_INFERENCE = {
  [0.2]: {
    min: 0,
    max: 10000,
  },
  [0.4]: {
    min: 10000,
    max: 15000,
  },
  [0.6]: {
    min: 15000,
    max: 30000,
  },
  [0.8]: {
    min: 30000,
    max: 1000000,
  },
};

export const DEAL_INFERENCE_WEIGHT = {
  CLIENT_TPV: 0.6,
  CLIENT_SEGMENT: 0.3,
  CLIENT_LOCATION: 0.3,
  USER_SALES_XP: 0.6,
};

export const SALES_XP_RANGE_INFERENCE = {
  [UserSalesXpEnum.BEGINNER]: 40,
  [UserSalesXpEnum.INTERMEDIATE]: 60,
  [UserSalesXpEnum.ADVANCED]: 80,
  [UserSalesXpEnum.EXPERT]: 100,
};
