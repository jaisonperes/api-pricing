import { ClientEntity } from 'src/app/clients/entities';
import { UserSalesXpEnum } from 'src/app/users/enum/user-sales-xp.enum';

export type CalculateMaxDiscountParams = {
  client: ClientEntity;
  salesXp: keyof typeof UserSalesXpEnum;
};
