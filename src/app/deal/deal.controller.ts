import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
  Request,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { DealService } from './deal.service';
import { SaveDealDto } from './dto/save-deal.dto';

@Controller('deal')
@UseGuards(AuthGuard('jwt'))
@ApiTags('Deal')
@ApiBearerAuth()
export class DealController {
  constructor(private readonly dealService: DealService) {}

  @Post()
  async save(@Body() body: SaveDealDto, @Request() req: any) {
    const { id } = req.user;
    return this.dealService.save(body, id);
  }

  @Get()
  async findAll(@Request() req: any) {
    const { id } = req.user;
    return this.dealService.findAll({ id });
  }
}
