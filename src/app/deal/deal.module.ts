import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientEntity } from '../clients/entities';
import { ProductEntity } from '../products/product.entity';
import { UserEntity } from '../users/users.entity';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../users/users.service';
import { DealController } from './deal.controller';
import { DealEntity } from './deal.entity';
import { DealService } from './deal.service';

@Module({
  imports: [
    UsersModule,
    TypeOrmModule.forFeature([
      DealEntity,
      ClientEntity,
      ProductEntity,
      UserEntity,
    ]),
  ],
  controllers: [DealController],
  providers: [DealService, UsersService],
})
export class DealModule {}
