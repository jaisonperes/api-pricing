import { Test, TestingModule } from '@nestjs/testing';
import { DealController } from '../deal.controller';
import { DealEntity } from '../deal.entity';
import { DealService } from '../deal.service';
import { SaveDealDto } from '../dto/save-deal.dto';

describe('DealController', () => {
  let dealController: DealController;
  let dealService: DealService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DealController],
      providers: [
        {
          provide: DealService,
          useValue: {
            save: jest.fn(),
          },
        },
      ],
    }).compile();

    dealController = module.get<DealController>(DealController);
    dealService = module.get<DealService>(DealService);
  });

  it('should be defined', () => {
    expect(dealController).toBeDefined();
    expect(dealService).toBeDefined();
  });

  describe('save', () => {
    it('should save a new deal with success', async () => {
      const body: SaveDealDto = {
        description: 'Test deal',
        price: 100,
        clientId: '',
        productId: '',
      };
      const dealEntityMock = { ...body } as unknown as DealEntity;
      jest.spyOn(dealService, 'save').mockResolvedValueOnce(dealEntityMock);

      const result = await dealController.save(body, 'user-id');

      expect(result).toBeDefined();
      expect(dealService.save).toBeCalledTimes(1);
    });
  });
});
