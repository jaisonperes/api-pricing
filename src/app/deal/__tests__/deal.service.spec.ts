import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DealEntity } from '../deal.entity';
import { DealService } from '../deal.service';
import { SaveDealDto } from '../dto/save-deal.dto';

describe('DealService', () => {
  let dealService: DealService;
  let dealRepository: Repository<DealEntity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DealService,
        {
          provide: getRepositoryToken(DealEntity),
          useValue: {
            create: jest.fn(),
            save: jest.fn(),
          },
        },
      ],
    }).compile();

    dealService = module.get<DealService>(DealService);
    dealRepository = module.get<Repository<DealEntity>>(
      getRepositoryToken(DealEntity),
    );
  });

  it('should be defined', () => {
    expect(dealService).toBeDefined();
    expect(dealRepository).toBeDefined();
  });

  describe('save', () => {
    it('should save a new deal with success', async () => {
      const data: SaveDealDto = {
        description: 'Test deal',
        price: 100,
      };
      const dealEntityMock = { ...data } as DealEntity;
      jest.spyOn(dealRepository, 'create').mockReturnValueOnce(dealEntityMock);
      jest.spyOn(dealRepository, 'save').mockResolvedValueOnce(dealEntityMock);

      const result = await dealService.save(data, 'user-id');

      expect(result).toBeDefined();
      expect(dealRepository.create).toBeCalledTimes(1);
    });
  });
});
