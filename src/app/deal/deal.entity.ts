import { Exclude } from 'class-transformer';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ClientEntity } from '../clients/entities';
import { ProductEntity } from '../products/product.entity';
import { UserEntity } from '../users/users.entity';
import { DealStatusEnum } from './enum/deal-status.enum';

@Entity({ name: 'deals' })
export class DealEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt: Date;

  @Column()
  price: number;

  @Column()
  description: string;

  @Column('enum', { enum: DealStatusEnum })
  status: DealStatusEnum = DealStatusEnum.PENDING;

  // Trading profitability rating: 0 - 100
  @Column({ name: 'profitability_rating', nullable: true })
  profitabilityRating: number;

  @Column({ name: 'max_discount', nullable: true, type: 'float' })
  maxDiscount: number;

  /**
   * Relations
   */

  @ManyToOne(() => UserEntity, (user) => user.deals, { eager: false })
  @Exclude({ toPlainOnly: true })
  user: UserEntity;

  @ManyToOne(() => ClientEntity, (client) => client.deal, { eager: true })
  @Exclude({ toPlainOnly: true })
  client: ClientEntity;

  @ManyToOne(() => ProductEntity, (product) => product.deals, { eager: true })
  @Exclude({ toPlainOnly: true })
  product: ProductEntity;
}
