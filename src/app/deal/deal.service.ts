import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ClientEntity } from '../clients/entities';
import { ProductEntity } from '../products/product.entity';
import { UsersService } from '../users/users.service';
import { DealEntity } from './deal.entity';
import { SaveDealDto } from './dto/save-deal.dto';
import {
  DEAL_INFERENCE_WEIGHT,
  SALES_XP_RANGE_INFERENCE,
  TPV_RANGE_INFERENCE,
} from './rules';
import { CalculateMaxDiscountParams } from './types/deal-types';

@Injectable()
export class DealService {
  constructor(
    @InjectRepository(DealEntity)
    private readonly dealRepository: Repository<DealEntity>,
    @InjectRepository(ClientEntity)
    private readonly clientRepository: Repository<ClientEntity>,
    @InjectRepository(ProductEntity)
    private readonly productRepository: Repository<ProductEntity>,
    private readonly userService: UsersService,
  ) {}

  private async calculateMaxDiscount({
    client,
    salesXp,
  }: CalculateMaxDiscountParams): Promise<number> {
    const clientTpv = client.tpv;
    const tpvInference = Object.keys(TPV_RANGE_INFERENCE).find(
      (key) =>
        clientTpv >= TPV_RANGE_INFERENCE[key].min &&
        clientTpv < TPV_RANGE_INFERENCE[key].max,
    );
    const clientLocationInference = client.location.inference / 100;
    const clientSegmentInference = client.segment.inference / 100;
    const salesXpInference = SALES_XP_RANGE_INFERENCE[salesXp] / 100;

    const clientTpvParsed =
      DEAL_INFERENCE_WEIGHT.CLIENT_TPV * parseFloat(tpvInference);
    const clientSegmentParsed =
      DEAL_INFERENCE_WEIGHT.CLIENT_SEGMENT * clientSegmentInference;
    const clientLocationParsed =
      DEAL_INFERENCE_WEIGHT.CLIENT_LOCATION * clientLocationInference;
    const userSalesXpParsed =
      DEAL_INFERENCE_WEIGHT.USER_SALES_XP * salesXpInference;

    console.log('tpv', tpvInference, clientTpvParsed);
    console.log('segment', clientSegmentInference, clientSegmentParsed);
    console.log('location', clientLocationInference, clientLocationParsed);
    console.log('salesXp', salesXpInference, userSalesXpParsed);

    const maxDiscount =
      clientTpvParsed +
      clientSegmentParsed +
      clientLocationParsed +
      userSalesXpParsed;

    return Math.round(maxDiscount * 100) / 100;
  }

  async save(data: SaveDealDto, userId: string): Promise<DealEntity> {
    const createdAt = new Date();

    const { experience } = await this.userService.getUserSalesXp(userId);

    const client = await this.clientRepository.findOne({
      where: {
        id: data.clientId,
      },
    });

    if (!client) {
      throw new BadRequestException('Client not found');
    }

    const product = await this.productRepository.findOne({
      where: {
        id: data.productId,
      },
    });

    if (!product) {
      throw new BadRequestException('Product not found');
    }

    const maxDiscount = await this.calculateMaxDiscount({
      client,
      salesXp: experience,
    });

    console.log(maxDiscount.toFixed(2));

    return this.dealRepository.save(
      this.dealRepository.create({
        ...data,
        createdAt,
        updatedAt: createdAt,
        user: { id: userId },
        client: { id: data.clientId },
        product: { id: data.productId },
        maxDiscount,
      }),
    );
  }

  async findAll({ id }: { id: string }): Promise<DealEntity[]> {
    return this.dealRepository.find({
      where: {
        user: { id },
      },
    });
  }
}
