import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { MessagesHelper } from 'src/helpers';

export class SaveDealDto {
  @IsNotEmpty({
    message: MessagesHelper.DEAL_DESCRIPTION_REQUIRED,
  })
  @ApiProperty({
    type: String,
    description: 'Deal description',
    example: 'Este é um ótimo acordo',
  })
  description: string;

  @IsNotEmpty({
    message: MessagesHelper.DEAL_PRICE_REQUIRED,
  })
  @IsNumber()
  @ApiProperty({
    type: Number,
    description: 'Deal price',
    example: 10000,
  })
  price: number;

  @IsNotEmpty({
    message: MessagesHelper.DEAL_CLIENT_REQUIRED,
  })
  @ApiProperty({
    type: String,
    description: 'Deal client uuid',
    example: '12345678-1234-1234-1234-1234567890ab',
  })
  clientId: string;

  @IsNotEmpty({
    message: MessagesHelper.DEAL_PRODUCT_REQUIRED,
  })
  @ApiProperty({
    type: String,
    description: 'Deal product uuid',
    example: '12345678-1234-1234-1234-1234567890ab',
  })
  productId: string;
}
