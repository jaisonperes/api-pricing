import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from './app/auth/auth.module';
import { DealModule } from './app/deal/deal.module';
import { PnlModule } from './app/pnl/pnl.module';
import { PromoModule } from './app/promo/promo.module';
import { configValidationSchema } from './config.schema';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './app/users/users.module';
import { ClientsModule } from './app/clients/clients.module';
import { ProductsModule } from './app/products/products.module';

const DB_HOST = 'DB_HOST';
const DB_PORT = 'DB_PORT';
const DB_USERNAME = 'DB_USERNAME';
const DB_PASSWORD = 'DB_PASSWORD';
const DB_DATABASE = 'DB_DATABASE';
const STAGE = 'STAGE';
const PROD_STAGE = 'prod';
const DEV_STAGE = 'dev';
const DB_CONNECTION_TYPE = 'postgres';
const ENV_DEV_FILE = '.env.dev';
const ENTITIES_PATH = '/**/*.entity{.ts,.js}';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: process.env.STAGE != DEV_STAGE,
      envFilePath: [ENV_DEV_FILE],
      validationSchema: configValidationSchema,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const isProduction = configService.get(STAGE) === PROD_STAGE;
        return {
          ssl: isProduction,
          extra: {
            ssl: isProduction ? { rejectUnauthorized: false } : null,
          },
          type: DB_CONNECTION_TYPE,
          autoLoadEntities: true,
          synchronize: !isProduction,
          host: configService.get(DB_HOST),
          port: configService.get(DB_PORT),
          username: configService.get(DB_USERNAME),
          password: configService.get(DB_PASSWORD),
          database: configService.get(DB_DATABASE),
          entities: [__dirname + ENTITIES_PATH],
          migrations: [__dirname + '/**/*.migration{.ts,.js}'],
          migrationsTableName: 'migrations',
        };
      },
    }),
    AuthModule,
    DealModule,
    PnlModule,
    PromoModule,
    UsersModule,
    ClientsModule,
    ProductsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
