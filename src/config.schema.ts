import * as Joi from '@hapi/joi';

const TTL_1_HOUR = 60 * 60;

const JWT_EXPIRES_IN = '3h';

export const configValidationSchema = Joi.object({
  // APP CONFIGURATION
  PORT: Joi.number().default(3000),
  STAGE: Joi.string().default(''),
  LOG_LEVEL: Joi.string().valid('debug', 'error').default('error'),
  // DATABASE CONFIGURATION
  DB_HOST: Joi.string().required(),
  DB_PORT: Joi.number().default(5432).required(),
  DB_USERNAME: Joi.string().required(),
  DB_PASSWORD: Joi.string().required(),
  DB_DATABASE: Joi.string().required(),
  // JWT CONFIGURATION
  JWT_SECRET: Joi.string().required(),
  JWT_ISSUER: Joi.string().required(),
  JWT_EXPIRES: Joi.string().default(JWT_EXPIRES_IN),
  // REDIS CONFIGURATION
  REDIS_TTL: Joi.string().default(TTL_1_HOUR),
  REDIS_HOST: Joi.string().required(),
  REDIS_PORT: Joi.number().default(6379).required(),
  REDIS_PASSWORD: Joi.string().required(),
  // USER CONFIGURATION
  SALT_ROUNDS: Joi.number().default(10),
});
