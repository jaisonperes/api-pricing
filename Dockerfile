FROM node:14.17.1-alpine as builder
RUN mkdir /app
WORKDIR /app
ADD . /app
RUN yarn
RUN yarn build

FROM node:14.17.1-alpine
COPY --from=builder /app /app
WORKDIR /app/dist
CMD ["yarn", "start"]