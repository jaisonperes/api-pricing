![](https://i.imgur.com/8qmKu9L.png)

# Api Pricing

## Description
This is a api, build with NestJs Framework to provide a negotiation tool for Stone challenge.

## Requirements

### 1. Environment Variables
Copy the environment variables file and adjust their values

```bash
cp -r .env.example .env.dev
```

### 2. JWT

Generate de private key and insert on JWT_SECRET env variable

```bash
openssl rand -base64 32
```

### 3. Docker and Docker Compose

Clone and go to root project folder and then, for up the containers, execute:
```sh
docker-compose up
```

For deatached mode, execute:

```sh
docker-compose up -d
```

For stop containers, execute:
```sh
docker-compose down
```

### Requirements for self hosted environment
#### 1. PostgreSQL
Run PostgreSQL Container if not have one locally

```bash
docker run --rm -d \
  --name pricing-postgres \
  -p 5432:5432 \
  -e POSTGRES_PASSWORD=123456 \
  -e POSTGRES_USER=postgres \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v /custom/mount:/var/lib/postgresql/data \
  postgres
```

#### 2. Redis
Run Redis Container if not have one locally

```bash
docker run --rm -d \    
  -h redis \
  -p 6380:6379 \
  -e REDIS_PASSWORD=123456 \
  -v redis-data:/data \
  --name redis-local \
  --restart always \
  redis:5.0.5-alpine3.9 /bin/sh -c 'redis-server --appendonly yes --requirepass ${REDIS_PASSWORD}'
```

## Install application

```bash
$ yarn install
```

## Running the application

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```
## View the documentation
When starting the application, a local Swagger server will be started at [localhost:3000/docs](http://localhost:3000/docs)

To download the json file, go to [localhost:3000/docs-json](http://localhost:3000/docs-json), save the json raw file and import into your favorite HTTP client

## Execute tests

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Create a Deal Flow

1. Create user

```http
POST /users HTTP/1.1
Content-Type: application/json
Host: localhost:3000
Content-Length: 112

{
	"firstName": "John",
	"lastName": "Doe",
	"email": "johndoe@mail.com",
	"password": "someStrongPassword"
}
```

2. Authenticate user

```http
POST /auth HTTP/1.1
Content-Type: application/json
Host: localhost:3000
Content-Length: 66

{
	"email": "johndoe@mail.com",
	"password": "someStrongPassword"
}
```

3. Create client
```http
POST /clients HTTP/1.1
Content-Type: application/json
Authorization: Bearer <The Auth JWT Token Here>
Host: localhost:3000
Content-Length: 145

{
	"name": "Farmácia Global",
	"email": "contato@farmaciaglobal.com",
	"phone": "(061)982792498",
	"tpv": 100000,
}
```

4. Create a Product

```http
POST /products HTTP/1.1
Content-Type: application/json
Authorization: Bearer <The Auth JWT Token Here>
Host: localhost:3000
Content-Length: 206

{
	"name": "Pedra da Sorte",
	"description": "A Pedra da Sorte irá transformar seu negócio",
	"billingType": "RECURRENCE",
	"fabCostPerUnit": 15,
	"transportCostPerUnit": 5,
	"taxPerUnit": 2,
	"price": 45
}
```

5. Create a Deal

```http
POST /deal HTTP/1.1
Content-Type: application/json
Authorization: Bearer <The Auth JWT Token Here>
Host: localhost:3000
Content-Length: 157

{
	"description": "Este é o melhor acordo",
	"price": 100,
	"clientId": "4560851a-ffcb-422c-a1ca-08b284d01795",
	"productId": "ef214c38-9656-4a97-831a-b15dc59f8794"
}
```